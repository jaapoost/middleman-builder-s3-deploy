Middleman builder
=================

Scope
-----
This [Docker](http://docker.io) image contains the tools we use at [OSIMIS](http://www.osimis.io) to build our static websites (ruby, node & middleman).


Usage examples
--------------

from the folder where your Gemfile.rb file is, start this command:

```
sudo docker run --rm -v $(pwd)/:/project osimis/middleman-builder

```

your output will be available in `$(pwd)/build`